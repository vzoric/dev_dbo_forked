﻿namespace DBO.Common
{
    public static class Constants
    {
        public const string AdminRole = "admin";
        public const string CompanyRole = "company";
        public const string EmployeeRole = "employee";

        public const string AdminEmail = "admin@dbo.as";
        public const string TestEmplyee1 = "emp1@dbo.as";
        public const string TestEmplyee2 = "emp2@dbo.as";
        public const string TestCompany1 = "comp1@dbo.as";
        public const string TestCompany2 = "comp2@dbo.as";

        public const int CompaniesPageSize = 10;
    }
}
