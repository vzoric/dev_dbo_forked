﻿using DBO.Data.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DBO.Data.Repositories
{
    public class UserRepository
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        private UserManager<ApplicationUser> _userManager;

        public UserRepository()
        {
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_db));
        }

        public IEnumerable<ApplicationUser> GetUsersByRole(string roleName)
        {
            var query = _userManager.Users.AsQueryable().Include("Company");
            return query.ToList().Where(x => _userManager.IsInRole(x.Id, Common.Constants.EmployeeRole));
        }

        public async Task<ApplicationUser> FindAsync(string name)
        {
            return await _userManager.FindByNameAsync(name);
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
