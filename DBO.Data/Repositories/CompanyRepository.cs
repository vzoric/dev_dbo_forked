﻿using System.Collections.Generic;
using System.Linq;
using DBO.Common;
using DBO.Data.ViewModels;
using System;
using DBO.Data.Models;
using System.Linq.Expressions;
using X.PagedList;

namespace DBO.Data.Repositories
{
    public class CompanyRepository
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        public List<CompanyViewModel> GetCompanies(string search, string searchCity, out bool hasMore, int pageNumber = 0)
        {
            hasMore = false;
            var query = _db.Companies.AsQueryable();
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(c => c.Name.Contains(search) /*|| c.TextDescription.Contains(search)*/);
            }
            if (!string.IsNullOrEmpty(searchCity))
            {
                query = query.Where(c => c.City.Contains(searchCity));
            }

            var result = new List<CompanyViewModel>();
            var pageSize = Constants.CompaniesPageSize;
            foreach (var company in query.OrderBy(x => x.Id).Skip(pageNumber * pageSize).Take(pageSize + 1))
            {
                result.Add(new CompanyViewModel(company));
            }

            if (result.Count > pageSize)
            {
                result.RemoveAt(pageSize);
                hasMore = true;
            }

            return result;
        }

        public IEnumerable<Company> GetAll(Expression<Func<Company, bool>> predicate = null)
        {
            var query = _db.Companies.AsQueryable();

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            //TODO: Fix this
            return query.Take(100).ToList();
        }

        public Select2PagedModel GetSelect2(int pageSize, int page, string term)
        {
            var result = new Select2PagedModel();

            var items = (from p in _db.Companies
                         where term == null || term == "" || p.Name.ToLower().Contains(term.ToLower())
                         orderby p.Id
                         select new Select2Model
                         {
                             id = p.Id.ToString(),
                             text = p.Name
                         }).ToPagedList(page, pageSize);

            result.TotalCount = items.TotalItemCount;
            result.Items = items.ToList();

            //var items = _db.Companies.Where(p => term == null || term == "" || p.Name.ToLower().Contains(term.ToLower())).AsEnumerable();

            //result.Items = items.Skip(pageSize * (page - 1))
            //    .Take(pageSize)
            //    .Select(e => new Select2Model
            //    {
            //        id = e.Id.ToString(),
            //        text = e.Name
            //    }).ToList();

            //result.TotalCount = items.Count();

            return result;
        }
    }
}
