﻿using DBO.Common.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBO.Data.ViewModels
{
    public class CreateCompanyViewModel
    {
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Email", ResourceType = typeof(Resources))]
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Phone", ResourceType = typeof(Resources))]
        [Required]
        public string Phone { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Resources))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources))]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
