﻿using DBO.Data.Models;

namespace DBO.Data.ViewModels
{
    public class CompanyViewModel
    {
        public CompanyViewModel()
        {
            
        }

        public CompanyViewModel(Company company)
        {
            this.Id = company.Id;
            this.Name = company.Name;
            this.Connections = company.Connections;
            this.Image = company.Image;
            this.TextDescription = company.TextDescription;
            this.Address1 = company.Address;
            this.Address2 = company.PostCode + " " + company.City;
            this.Phone = company.Phone;
        }

        public int Id { get; set; }
        public int Connections { get; set; }
        public string Image { get; set; }
        public string TextDescription { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
    }
}