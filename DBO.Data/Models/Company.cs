﻿using System.Collections.Generic;
using DBO.Common.Resources;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBO.Data.Models
{
    public class Company
    {
        public int Id { get; set; }
        public int CVR { get; set; }

        [Index]
        [MaxLength(255)]
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        public string Name { get; set; }

        [MaxLength(255)]
        [Display(Name = "Address", ResourceType = typeof(Resources))]
        public string Address { get; set; }

        [MaxLength(15)]
        [Display(Name = "PostCode", ResourceType = typeof(Resources))]
        public string PostCode { get; set; }

        [Index]
        [MaxLength(255)]
        [Display(Name = "City", ResourceType = typeof(Resources))]
        public string City { get; set; }

        [MaxLength(15)]
        [Display(Name = "Phone", ResourceType = typeof(Resources))]
        public string Phone { get; set; }

        [MaxLength(255)]
        [Display(Name = "Web", ResourceType = typeof(Resources))]
        public string Web { get; set; }

        [MaxLength(255)]
        [Display(Name = "Email", ResourceType = typeof(Resources))]
        public string Email { get; set; }

        [MaxLength(255)]
        [Display(Name = "PersonName", ResourceType = typeof(Resources))]
        public string PersonName { get; set; }

        [MaxLength(255)]
        [Display(Name = "Chairman", ResourceType = typeof(Resources))]
        public string Chairman { get; set; }

        [Display(Name = "IndustryCode", ResourceType = typeof(Resources))]
        public int? IndustryCode { get; set; }

        [MaxLength(255)]
        [Display(Name = "IndustryText", ResourceType = typeof(Resources))]
        public string IndustryText { get; set; }

        [Display(Name = "AdvertisingProtection", ResourceType = typeof(Resources))]
        public bool AdvertisingProtection { get; set; }

        [MaxLength(255)]
        [Display(Name = "Owner", ResourceType = typeof(Resources))]
        public string Owner { get; set; }

        [MaxLength(255)]
        [Display(Name = "Image", ResourceType = typeof(Resources))]
        public string Image { get; set; }

        [MaxLength(255)]
        [Display(Name = "TextDescription", ResourceType = typeof(Resources))]
        public string TextDescription { get; set; }

        [Display(Name = "Connections", ResourceType = typeof(Resources))]
        public int Connections { get; set; }


        public virtual ICollection<CompanySkill> CompanySkills { get; set; }
        public virtual ICollection<ApplicationUser> Employees { get; set; }
    }
}