﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DBO.Common.Resources;

namespace DBO.Data.Models
{
    public class Skill
    {
        public int Id { get; set; }
        [Display(Name = "Name", ResourceType = typeof(Resources))]
        public string Name { get; set; }

        public virtual ICollection<CompanySkill> CompanySkills { get; set; }
    }
}
