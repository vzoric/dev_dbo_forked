﻿using DBO.Common;
using DBO.Data;
using DBO.Data.Models;
using DBO.Data.Repositories;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DBO.Controllers
{
    [Authorize(Roles = Constants.AdminRole)]
    public class CompaniesController : BaseController
    {
        private ApplicationDbContext db;
        private readonly CompanyRepository _companyRepository;
        private readonly UserRepository _userRepository;

        public CompaniesController()
        {
            db = new ApplicationDbContext();
            _companyRepository = new CompanyRepository();
            _userRepository = new UserRepository();
        }

        // GET: Companies
        public ActionResult Index(string search)
        {
            ViewBag.Search = search;
            var companies = db.Companies.AsEnumerable();
            if (!string.IsNullOrEmpty(search))
            {
                companies = companies.Where(c => c.Name.ToLower().Contains(search.ToLower()));
            }

            return View(companies.Take(100).ToList());
        }

        // GET: Companies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }

            var skills = db.CompanySkills.Include(s => s.Skill).Where(s => s.CompanyId == id);
            company.CompanySkills = skills.ToList();

            return View(company);
        }

        // GET: Companies/Admin/5
        public ActionResult Admin(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }

            var skills = db.CompanySkills.Include(s => s.Skill).Where(s => s.CompanyId == id);
            company.CompanySkills = skills.ToList();

            return View(company);
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,PostCode,Image,Address,City,Phone,TextDescription,Connections")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Companies.Add(company);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(company);
        }

        // GET: Companies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,PostCode,Image,Address,City,Phone,TextDescription,Connections")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(company);
        }

        // GET: Companies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.Companies.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Company company = db.Companies.Find(id);
            db.Companies.Remove(company);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Employments()
        {
            var employees = _userRepository.GetUsersByRole(Constants.EmployeeRole);
            return View(employees);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateEmployment(string username, int? companies)
        {
            if(string.IsNullOrEmpty(username))
            {
                return View("Error");
            }

            var user = await _userRepository.FindAsync(username);
            user.CompanyId = companies;

            await _userRepository.SaveAsync();
            return RedirectToAction("Employments");
        }

        public ActionResult CompanySelect2(int pageSize, int page, string term)
        {
            var model = _companyRepository.GetSelect2(pageSize, page, term);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
