﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBO.Common.Resources;
using DBO.Data;
using System.Linq;
using System.Web.Mvc;
using DBO.Data.Repositories;
using System.Linq;
using System.Web.Mvc;

namespace DBO.Controllers
{
    public class HomeController : BaseController
    {
        private CompanyRepository _companyRepository = new CompanyRepository();

        public ActionResult Index(string search, string searchCity)
        {
            var companies = _companyRepository.GetCompanies(search, searchCity, out var hasMore);

            ViewBag.Search = search;
            ViewBag.SearchCity = searchCity;
            ViewBag.HasMoreResults = hasMore;
            return View(companies);
        }

        public ActionResult ReadMore(string search, string searchCity, int pageNumber)
        {
            ViewBag.Search = search;
            ViewBag.SearchCity = searchCity;
            var companies = _companyRepository.GetCompanies(search, searchCity, out var hasMore, pageNumber);
            Response.Headers["X-HasMoreResults"] = hasMore.ToString().ToLower();

            return PartialView("_ReadMoreCompaniesPartial", companies);
        }

        public ActionResult SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
            {
                cookie.Value = culture;   // update cookie value
            }
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }

            Response.Cookies.Add(cookie);

            return RedirectToAction("Index");
        }
    }
}