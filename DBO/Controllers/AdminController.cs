﻿using System.Web.Mvc;
using DBO.Common;

namespace DBO.Controllers
{
    [Authorize(Roles = Constants.AdminRole)]
    public class AdminController : BaseController
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
    }
}